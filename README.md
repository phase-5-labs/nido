# Nido

A temperature and relative humidity sensor running on a [Blynk local server](https://github.com/blynkkk/blynk-server).

- Sensing Device: [Arduino](https://www.arduino.cc/) Uno + Ethernet Shield + AM2302 Sensor
- Server: [Beaglebone](https://beagleboard.org/bone-original) running on [Debian](https://www.debian.org/) Linux

Project page: https://www.hackster.io/foundationer/temperature-and-humidity-sensor-on-a-self-hosted-cloud-6d068f
