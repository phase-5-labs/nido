/*************************************************************
  Download latest Blynk library here:
    https://github.com/blynkkk/blynk-library/releases/latest

  Blynk is a platform with iOS and Android apps to control
  Arduino, Raspberry Pi and the likes over the Internet.
  You can easily build graphic interfaces for all your
  projects by simply dragging and dropping widgets.

    Downloads, docs, tutorials: http://www.blynk.cc
    Sketch generator:           http://examples.blynk.cc
    Blynk community:            http://community.blynk.cc
    Follow us:                  http://www.fb.com/blynkapp
                                http://twitter.com/blynk_app

  Blynk library is licensed under MIT license
  This example code is in public domain.

 *************************************************************

  This example shows how value can be pushed from Arduino to
  the Blynk App.

  WARNING :
  For this example you'll need Adafruit DHT sensor libraries:
    https://github.com/adafruit/Adafruit_Sensor
    https://github.com/adafruit/DHT-sensor-library

  App project setup:
    Value Display widget attached to V5
    Value Display widget attached to V6
 *************************************************************/

/* Comment this out to disable prints and save space */
#define BLYNK_DEBUG
#define BLYNK_PRINT Serial

#include <SPI.h>
#include <Ethernet.h>
#include <BlynkSimpleEthernet.h>
#include <DHT.h>
#include <SevSeg.h>

#define W5100_CS  10
#define SDCARD_CS 4
#define DHTPIN 2          // What digital pin we're connected to

// Uncomment whatever type you're using!
//#define DHTTYPE DHT11     // DHT 11
#define DHTTYPE DHT22   // DHT 22, AM2302, AM2321
//#define DHTTYPE DHT21   // DHT 21, AM2301

DHT dht(DHTPIN, DHTTYPE);
BlynkTimer timer;
SevSeg sevSeg;

// This function sends Arduino's up time every second to Virtual Pin (5).
// In the app, Widget's reading frequency should be set to PUSH. This means
// that you define how often to send data to Blynk App.
void sendSensor() {
  float humidity = dht.readHumidity();
  float temperature = dht.readTemperature(); // or dht.readTemperature(true) for Fahrenheit
  //float f = dht.readTemperature(true);

  if (isnan(humidity) || isnan(temperature)) {
    Serial.println("Failed to read data from AM2302 sensor!");
    return;
  }

  sevSeg.setNumber(temperature,1);

  // You can send any value at any time.
  // Please don't send more that 10 values per second.
  Blynk.virtualWrite(V5, humidity);
  Blynk.virtualWrite(V6, temperature);
  //Blynk.virtualWrite(V7, f);
}

void setup()
{
  // Debug console
  Serial.begin(9600);
  
  setupAnalogPins();
  setupEthernetShield();
  setupDisplay();
  setupBlynk();

  dht.begin();

  // Setup a function to be called every 3 seconds
  timer.setInterval(30000L, sendSensor);
}

void setupAnalogPins() {
  pinMode(A0, OUTPUT);
  pinMode(A1, OUTPUT);
  pinMode(A2, OUTPUT);
}

void setupEthernetShield() {
  pinMode(SDCARD_CS, OUTPUT);
  digitalWrite(SDCARD_CS, HIGH); // Deselect the SD card  
}

void setupDisplay() {
  byte numDigits = 4;
  byte digitPins[] = { 11, 12, 13, A2};
  byte segmentPins[] = {3, A0, 5, 6, 7, 8, 9, A1};
  bool resistorsOnSegments = false;
  byte hardwareConfig = COMMON_CATHODE;
  bool updateWithDelay = false;
  bool leadingZeros = false;
  bool disableDecPoint = false;
  sevSeg.begin(hardwareConfig, numDigits, digitPins, segmentPins, resistorsOnSegments, updateWithDelay, leadingZeros, disableDecPoint);
  sevSeg.setBrightness(90);  
}

void setupBlynk() {
  // You should get Auth Token in the Blynk App.
  // Go to the Project Settings (nut icon).
  char auth_server[] = "YOUR AUTH TOKEN HERE";

  IPAddress server_ip (192, 168, 1, 144);
  IPAddress arduino_ip  ( 192,   168,   1,  142);
  IPAddress dns_ip      ( 1,   1,   1,   1);      // Cloudflare DNS server
  IPAddress gateway_ip  ( 192,   168,   0,   1);
  IPAddress subnet_mask ( 255, 255, 255,   0);

  byte arduino_mac[] =  { 0xEF, 0x3A, 0x4E, 0x6A, 0x7D, 0xDE };

  Blynk.begin(auth_server, server_ip, 8080, arduino_ip, dns_ip, gateway_ip, subnet_mask, arduino_mac);  
}

void loop()
{
  Blynk.run();
  timer.run();
  sevSeg.refreshDisplay();
}
